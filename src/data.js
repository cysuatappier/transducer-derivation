//dummy data
export const data1 = [
  {
    id: 'id_1',
    cost: 1,
    action: 2,
    click: 30,
  },
  //....
];

export const data2 = [
  {
    id: 'id_2',
    cost: 1,
    action: 2,
    impression: 500,
  },
  //....
]

export const data3 = [
  {
    id: 'id_3',
    action: 2,
    click: 30,
  },
  //....
]