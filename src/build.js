
// Make our derivation easier. consider the following scenario.
// We have an array, and we want to modify it to get new value.
const arr = [1, 2, 3];

//f1, f2, f3 are some variable functions will apply to our array.
const f1 = (x) => x+1;
const f2 = (x) => x*2;
const f3 = (x) => x*x;

// We usually evaluate functions one by one by mapping to get our final answer.
const res1 = arr.map(f1).map(f2).map(f3);

// However, it will generate many intermidate arrays.
// When array is large, the above function will have poor performance.

// We want to visit each element only once, and get a sinle result of each element and put it into new array.
// So we write the following code.
const res2 = arr.reduce(comp1(f1, f2, f3), [])

// You may notice we use reduce function instead map.
// The reason is that reduce (foldLeft) is actually the most powerful array (or other collection) operations.
// Operations like map, filter and reverse can be defined in terms of reduce.

// Question: How to write a comp1 to make res1 === res2 ?

// Ans:
const comp1 = (f1, f2, f3) =>{
  return (result, x) =>{
    return result.push(f3(f2(f1(x))))
  }
}

// It is quite close to what we discussed before (f*g)(x) = f(g(x)).
// I will explain above code one by one.

//const comp1(f1, f2, f3) will return a function, we call them compositedFn

const compositedFn = (result, x) => {
  return result.push(f3(f2(f1(x))))
}

// we substitute f1(x) = x+1 into equation.

const compositedFn = (result, x) => {
  return result.push(f3(f2(x+1)))
}

// we substitute f2(x) = x*2 into equation.

const compositedFn = (result, x) => {
  return result.push(f3((x+1)*2))
}

// we substitute f3(x) = x*x into equation.

const compositedFn = (result, x) => {
  return result.push(((x+1)*2)*((x+1)*2))
};

//Therefore, the original reduce function became

const res2 = arr.reduce((result, x) => {
  const calculatedNumber = ((x+1)*2) * ((x+1)*2);
  return result.push(calculatedNumber);
}, [])


// But comp1 is not flexible enough.
// What if we want compose function arbitarily.
// Like following code.

const comp2 = ???

arr.reduce(comp2(f1), [])
arr.reduce(comp2(f1)(comp2(f2)), [])
arr.reduce(comp2(f1)(comp2(f2)(comp2(f3))), [])

// Actually, we need to tell comp2 when to stop waiting for new functions and
// start to calculate the result
// The comp2 will be look like this one
// comp2(f1)(endFn)
// comp2(f2)(comp2(f1)(endFn))
// comp2(f1)(comp2(f2)(comp2(f3)(endFn)))

const endFn = ???

// Question: write comp2 and endFn

// Ans:
const comp2 = (f) => (combine) => (result, x) => {
  return combine(result, f(x))
}

const endFn = (result, x) => {
  return result.push(x)
}

// comp2 is a function receive a function and waiting for a combine function.
// Once it received combined function, it return a reduce function.
// That reduce function will use combine function to combine the result and transformed data (f(x)).
// We can call it mapper.

const mapper = comp2;

// endFn is the function which push our result of each item to final array.
// we can call it append;

const append = endFn;

// comp2 looks daunting, let us take a deep look.

// Let us to see how this work.
// Take the following function for example:

arr.reduce(mapper(f2)(mapper(f1)(append)), [])

// First, we evaluate the inner function
mapper(f1)(append) = (result, x) => append(result, f1(x))

// Second, we substitute it back to original function
mapper(f2)(mapper(f1)(append)) =

(result, x) => combine(result, f2(x))  // WHERE combine = append(result, f1(x))

// Finally, we can get
(result, x) => append(result, f2(f1(x)));

// Evaluate append
(result, x) => result.push(f2(f1(x)));

// In other words,
arr.reduce(mapper(f2)(mapper(f1)(append)), [])
// is equal to
arr.reduce((result, x) => result.push(f2(f1(x))), []);



// Conclusion:
// There are lots of libraries already build transducer for us.
// However, if you don't want to use those complex function. all you need is write down the following two function.

const mapper = (f) => (combine) => (result, x) => {
  return combine(result, f(x))
}

const append = (result, x) => {
  result.push(x);
  return result;
}

// then original function
const result = data1
  .map(calculateCPC)
  .map(calculateCPA)
  .map(calculateCVR);

// become
const result = data1.reduce(mapper(calculateCVR)(mapper(calculateCPA)(mapper(calculateCPC)(append))), [])

// If you feel the above function is ugly.
// Then you can write a composite function to composite it.

const compose = (...args) => {
  const start = args.length - 1;
  let result = args[start];
  for(let i = start-1; i>=0 ; i--){
    result = args[i](result);
  }
  return result;
}

const finalMapper = compose(mapper(calculateCVR), mapper(calculateCPA), mapper(calculateCPC), append);

const result = data1.reduce(finalMapper, []);

// Everything is done!!


