const exampleData = [
  {
    id: 'id_1',
    cost: 1,
    action: 2,
    click: 30,
  },
  {
    id: 'id_2',
    cost: 5,
    action: 10,
    click: 200,
  },
];

const mapper = (f) => (combine) => (result, x) => {
  return combine(result, f(x))
}

const append = (result, x) => {
  result.push(x);
  return result;
}

const calculateCPC = (d)=>{
  const {
    cost, click
  } = d;
  const cpc = click === 0 ? 0 : (cost/click);
  return Object.assign({cpc}, d);
}

const calculateCPA = (d)=>{
  const {
    cost, action
  } = d;
  const cpa = action === 0 ? 0 : (cost/action);
  return Object.assign({cpa}, d);
}

const calculateCVR = (d)=>{
  const {
    click, action,
  } = d;
  const cvr = click === 0 ? 0 : (action/click);
  return Object.assign({cvr}, d);
}

const calculateCPM = (d)=>{
  const {
    cost, impression,
  } = d;
  const cpm = impression === 0 ? 0 : (cost*1000/impression)
  return Object.assign({cpm}, d);
}

const compose = (...args) => {
  const start = args.length - 1;
  let result = args[start];
  for(let i = start-1; i>=0 ; i--){
    result = args[i](result);
  }
  return result;
}

const finalMapper = compose(mapper(calculateCVR), mapper(calculateCPA), mapper(calculateCPC), append);

const result = exampleData.reduce(finalMapper, [])

console.log('Original data:');
console.log(exampleData);
console.log('===================');

console.log('Result:')
console.log(result);