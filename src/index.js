

import {
  data1,
  data2,
  data3,
} from './data';

// Now, we have data, so we need to process data.

const processData1 = data1.map(d=>{
  const {
    ,cost click, action,
  } = d;
  d.cpc = click === 0 ? 0 : (cost/click);
  d.cpa = action === 0 ? 0 : (cost/action);
  d.cvr = click === 0 ? 0 : (action/click);
  return d;
});

const processData2 = data2.map(d=>{
  const {
    cost, action, impression,
  } = d;
  d.cpa = action === 0 ? 0 : (cost/action);
  d.cpm = impression === 0 ? 0 : (cost*1000/impression)
  return d;
});

const processData3 = data3.map(d=>{
  const {
    click, action,
  } = d;
  d.cvr = click === 0 ? 0 : (action/click);
  return d;
});




// Those work fine, but we want to reuse our code!
// So we write helper functions!

const calculateCPC = (d)=>{
  const {
    cost, click
  } = d;
  const cpc = click === 0 ? 0 : (cost/click);
  return Object.assign({cpc}, d);
}

const calculateCPA = (d)=>{
  const {
    cost, action
  } = d;
  const cpa = action === 0 ? 0 : (cost/action);
  return Object.assign({cpa}, d);
}

const calculateCVR = (d)=>{
  const {
    click, action,
  } = d;
  const cvr = click === 0 ? 0 : (action/click);
  return Object.assign({cvr}, d);
}

const calculateCPM = (d)=>{
  const {
    cost, impression,
  } = d;
  const cpm = impression === 0 ? 0 : (cost*1000/impression)
  return Object.assign({cpm}, d);
}


const processData4 = data1.map(calculateCPC).map(calculateCPA).map(calculateCVR);
const processData5 = data1.map(calculateCPA).map(calculateCPM);
const processData6 = data1.map(calculateCVR);


// Look great. We abstract those common logic and make our code simpler.
// However, this method has downside.
// Everytime you execute map function, it will generate intermediate array.

const result = data1
  .map(calculateCPC)
  .map(calculateCPA)
  .map(calculateCVR);

// if our original array has 100,000 items
// then map four times will generate 400,000 items, which is not good...


// Can we use those transformer function without pain ?
// Recall what we learn in math class.

(f*g)(x) = f(g(x));


// Can we composite our function like this one then apply to our data?
const compositedFunction1 = calculateCPC(calculateCPA(calculateCVR)); //ERROR!!
const result1 = data1.map(compositedFunction)

const compositedFunction2 = calculateCPA(calculateCPM); //ERROR!!
const result2 = data2.map(compositedFunction)

const compositedFunction3 = calculateCVR // This one work though ....
const result3 = data3.map(compositedFunction)

// We can not compose those function directly because they accept a data, not a function.
// Therefore, transducer come to rescue!

// Please read ./build.js to continue



