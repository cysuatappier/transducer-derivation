#!/usr/bin/python
# -*- coding: utf-8 -*-

# DERIVED FROM example.js

exampleData = [
  {
    'id': 'id_1',
    'cost': 1,
    'action': 2,
    'click': 30,
  },
  {
    'id': 'id_2',
    'cost': 5,
    'action': 10,
    'click': 200,
  },
]

# const mapper = (f) => (combine) => (result, x) => {
#   return combine(result, f(x))
# }

def mapper(f):
  # or:
  # return lambda combine: lambda result, x: combine(result, f(x))

  def ret(combine):
    def ret2(result, x):
      return combine(result, f(x))
    return ret2
  return ret

# const append = (result, x) => {
#   result.push(x);
#   return result;
# }

def append(result, x):
  result.append(x)
  return result

# const calculateCPC = (d)=>{
#   const {
#     cost, click
#   } = d;
#   const cpc = click === 0 ? 0 : (cost/click);
#   return Object.assign({cpc}, d);
# }

def calculateCPC(d):
  cost = float(d['cost'])
  click = float(d['click'])
  cpc = 0 if click == 0 else (cost/click)
  ret = {
    'cpc': cpc,
  }
  ret.update(d)
  return ret

# const calculateCPA = (d)=>{
#   const {
#     cost, action
#   } = d;
#   const cpa = action === 0 ? 0 : (cost/action);
#   return Object.assign({cpa}, d);
# }

def calculateCPA(d):
  cost = float(d['cost'])
  action = float(d['action'])
  cpa = 0 if action == 0 else (cost/action)
  ret = {
    'cpa': cpa,
  }
  ret.update(d)
  return ret

# const calculateCVR = (d)=>{
#   const {
#     click, action,
#   } = d;
#   const cvr = click === 0 ? 0 : (action/click);
#   return Object.assign({cvr}, d);
# }

def calculateCVR(d):
  click = float(d['click'])
  action = float(d['action'])
  cvr = 0 if click == 0 else (action/click)
  ret = {
    'cvr': cvr,
  }
  ret.update(d)
  return ret

# const calculateCPM = (d)=>{
#   const {
#     cost, impression,
#   } = d;
#   const cpm = impression === 0 ? 0 : (cost*1000/impression)
#   return Object.assign({cpm}, d);
# }

def calculateCPM(d):
  cost = float(d['cost'])
  impression = float(d['impression'])
  cpm = 0 if impression == 0 else (cost*1000/impression)
  ret = {
    'cpm': cpm,
  }
  ret.update(d)
  return ret

# const compose = (...args) => {
#   const start = args.length - 1;
#   let result = args[start];
#   for(let i = start-1; i>=0 ; i--){
#     result = args[i](result);
#   }
#   return result;
# }

def compose(*args):
  start = len(args) - 1
  result = args[start]
  for i in reversed(range(0, start)):
    result = args[i](result)
  return result

# const finalMapper = compose(mapper(calculateCVR), mapper(calculateCPA), mapper(calculateCPC), append);
finalMapper = compose(mapper(calculateCVR), mapper(calculateCPA), mapper(calculateCPC), append)

# const result = exampleData.reduce(finalMapper, [])
result = reduce(finalMapper, exampleData, [])

print 'Original data:'
print exampleData
print '==================='

print 'Result:'
print result
